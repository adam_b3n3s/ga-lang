#include "../include/ga_interpreter.hpp"

namespace ga {
    ga_interpreter::ga_interpreter(const std::vector<std::string>& files) : files(files) {
        for (const std::string& s : files) {
            std::ifstream file(s);
            if (!file.is_open()) {
                std::cerr << "Failed to open the file." << std::endl;
                return;
            }
            std::string line;
            while (std::getline(file, line)) {
                std::cout << line << std::endl;
            }
            file.close();
        }
    }
}
