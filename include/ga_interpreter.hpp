#include <vector>
#include <string>
#include <iostream>
#include <fstream>

namespace ga {
    class ga_interpreter {
        std::vector<std::string> files;
    public:
        ga_interpreter(const std::vector<std::string>& files);
    };
}
