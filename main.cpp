#include <iostream>
#include <fstream>
#include <string>
#include "./include/ga_interpreter.hpp"
#include <vector>
#include <string>


int main(int argc, char* argv[]) {
    if (argc > 1)
    {
        ga::ga_interpreter interp ( { &argv[1], &argv[argc-1] } );
        return 0;
    }
    return 1;
}
